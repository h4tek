#!/bin/bash
INPUT="rec/img%05d.tif"
OUTPUT="video.avi"
BITRATE="8000000"
FPS="30"
ffmpeg -r "$FPS" -i "$INPUT" -pass 1 -vcodec libx264 -b "$BITRATE" \
-flags +loop -cmp +chroma -partitions +parti4x4+partp8x8+partb8x8 -me_method dia \
-subq 1 -trellis 0 -refs 1 -bf 16 -b_strategy 1 -coder 1 \
-me_range 16 -g 250 -keyint_min 25 -sc_threshold 40 -i_qfactor 0.71 \
-bt "$BITRATE" -qcomp 0.6 -qmin 10 -qmax 51 -qdiff 4 -threads 0 "$OUTPUT"
ffmpeg -r "$FPS" -i "$INPUT" -pass 2 -vcodec libx264 -b "$BITRATE" \
-flags +loop -cmp +chroma -partitions +parti8x8+parti4x4+partp8x8+partp4x4+partb8x8 \
-flags2 +dct8x8+wpred+bpyramid+mixed_refs -me_method umh \
-subq 7 -trellis 1 -refs 6 -bf 16 -directpred 3 -b_strategy 1 \
-coder 1 -me_range 16 -g 250 -keyint_min 25 -sc_threshold 40 \
-i_qfactor 0.71 -bt "$BITRATE" -qcomp 0.6 -qmin 10 -qmax 51 -qdiff 4 \
-threads 0 -y "$OUTPUT"
