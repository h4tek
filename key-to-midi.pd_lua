local KeyToMIDI = pd.Class:new():register("key-to-midi")
local notes = {
  z = 36,
  s = 37,
  x = 38,
  d = 39,
  c = 40,
  v = 41,
  g = 42,
  b = 43,
  h = 44,
  n = 45,
  j = 46,
  m = 47,

  comma = 48,
  l = 49,
  period = 50,
  semicolon = 51,
  slash = 52,

  q = 48,
  D2 = 49,
  w = 50,
  D3 = 51,
  e = 52,
  r = 53,
  D5 = 54,
  t = 55,
  D6 = 56,
  y = 57,
  D7 = 58,
  u = 59,

  i = 60,
  D9 = 61,
  o = 62,
  D0 = 63,
  p = 64,
  bracketleft = 65,
  equal = 66,
  bracketright = 67,

  space = 0,
  BackSpace = -1
}

function KeyToMIDI:initialize(name, atoms)
  self.inlets = 1
  self.outlets = 2
  return true
end

function KeyToMIDI:in_1_symbol(s)
  if (notes[s]) then
    self:outlet(1, "float", { notes[s] })
  else
    self:outlet(2, "symbol", { s })
  end
end
