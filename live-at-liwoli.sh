#!/bin/bash
STARTTIME="$(date --iso=s)"
manypngtoppm 0 44999 "png/img%05d.tif.png" |
ppmtoy4m -S 444 -F 25:1 |
y4mscaler -I norm=pal -O preset=dvd -O yscale=576/768 |
mpeg2enc -f 8 -a 2 -q 6 -b 8000 -o out-dvd.m2v &&
mp2enc -b 224 -o out-dvd.m2a < audio.48000.wav &&
mplex -f 8 -V -o out-dvd.mpeg out-dvd.m2v out-dvd.m2a &&
manypngtoppm 0 44999 "png/img%05d.tif.png" |
ppmtoy4m -S 444 -F 25:1 |
y4mscaler -O size=SRC -O chromass=420MPEG2 |
mpeg2enc --no-constraints -f 3 -a 2 -q 6 -b 15000 -o out-hd.m2v &&
mp2enc -b 224 -o out-hd.m2a < audio.48000.wav &&
mplex -f 3 -V -o out-hd.mpeg out-hd.m2v out-hd.m2a &&
echo "$STARTTIME" &&
echo "$(date --iso=s)"
