#include <math.h>
#include <string.h>

#include <m_pd.h>

static t_class *h4tek_coords_class;

struct vector4 {
  float v[4];
};

struct matrix4 {
  float m[16];
};

inline static void diag4(struct matrix4 *m, const float *f) {
  memset(m, 0, sizeof(struct matrix4));
  float *n = m->m;
  n[ 0] = f[0];
  n[ 5] = f[1];
  n[10] = f[2];
  n[15] = f[3];
}

inline static void rot4(struct matrix4 *m, int i, int j, float a) {
  memset(m, 0, sizeof(struct matrix4));
  float *n = m->m;
  n[ 0] = 1.0;
  n[ 5] = 1.0;
  n[10] = 1.0;
  n[15] = 1.0;
  float c = cosf(a);
  float s = sinf(a);
  n[i*5]   =  c;
  n[j*5]   =  c;
  n[i*4+j] =  s;
  n[j*4+i] = -s;
}

inline static void mul4(struct matrix4 *m, const struct matrix4 *l, const struct matrix4 *r) {
  for (int i = 0; i < 4; ++i) {
    for (int j = 0; j < 4; ++j) {
      float f = 0.0;
      for (int k = 0; k < 4; ++k) {
        f += l->m[i*4+k] * r->m[k*4+j];
      }
      m->m[i*4+j] = f;
    }
  }
}

inline static void mul4v(
  struct vector4 *v, const struct matrix4 *l, const struct vector4 *r
) {
  for (int i = 0; i < 4; ++i) {
    float f = 0.0;
    for (int j = 0; j < 4; ++j) {
      f += l->m[i*4+j] * r->v[j];
    }
    v->v[i] = f;
  }
}

struct vector3 {
  float v[3];
};

#define H4TEK_COORDS_POINTS 256

struct h4tek_coords {
  t_object obj;
  t_outlet *outlet;
  float scale4[4];
  float rotate4[6];
  float persp43;
  struct matrix4 transform4;
  struct vector4 points[H4TEK_COORDS_POINTS];
};

static void h4tek_coords_bang(struct h4tek_coords *d) {
  struct matrix4 n1, n2, n3;
  diag4(&n1, d->scale4);
  rot4(&n2, 0, 1, d->rotate4[0]); mul4(&n3, &n2, &n1);
  rot4(&n2, 0, 2, d->rotate4[1]); mul4(&n1, &n2, &n3);
  rot4(&n2, 1, 2, d->rotate4[2]); mul4(&n3, &n2, &n1);
  rot4(&n2, 0, 3, d->rotate4[3]); mul4(&n1, &n2, &n3);
  rot4(&n2, 1, 3, d->rotate4[4]); mul4(&n3, &n2, &n1);
  rot4(&n2, 2, 3, d->rotate4[5]); mul4(&d->transform4, &n2, &n3);
  for (int c = 0; c < H4TEK_COORDS_POINTS; ++c) {
    struct vector4 v4o;
    mul4v(&v4o, &d->transform4, &d->points[c]);
    struct vector3 v3;
    float z = d->persp43 + v4o.v[3];
    for (int k = 0; k < 3; ++k) {
      v3.v[k] = v4o.v[k] / z;
    }
    t_atom out[4];
    SETFLOAT(&out[0], v3.v[0]);
    SETFLOAT(&out[1], v3.v[1]);
    SETFLOAT(&out[2], v3.v[2]);
    SETFLOAT(&out[3], c);
    outlet_list(d->outlet, &s_list, 4, out);
  }
}

static void h4tek_coords_scale4(
  struct h4tek_coords *d, t_symbol *s, int count, t_atom *atoms
) {
  for (int i = 0; i < 4; ++i) {
    d->scale4[i] = atoms[i].a_w.w_float;
  }
}

static void h4tek_coords_rotate4(
  struct h4tek_coords *d, t_symbol *s, int count, t_atom *atoms
) {
  for (int i = 0; i < 6; ++i) {
    d->rotate4[i] = atoms[i].a_w.w_float;
  }
}

static void h4tek_coords_persp43(
  struct h4tek_coords *d, t_symbol *s, int count, t_atom *atoms
) {
  d->persp43 = atoms[0].a_w.w_float;
}

static struct h4tek_coords *h4tek_coords_new(void) {
  struct h4tek_coords *d = (struct h4tek_coords *) pd_new(h4tek_coords_class);
  d->outlet = outlet_new(&d->obj, &s_list);
  struct vector4 dirs[16] = {
    {{  1,  1,  1,  1 }},
    {{  1,  1,  1, -1 }},
    {{  1,  1, -1,  1 }},
    {{  1,  1, -1, -1 }},
    {{  1, -1,  1,  1 }},
    {{  1, -1,  1, -1 }},
    {{  1, -1, -1,  1 }},
    {{  1, -1, -1, -1 }},
    {{ -1,  1,  1,  1 }},
    {{ -1,  1,  1, -1 }},
    {{ -1,  1, -1,  1 }},
    {{ -1,  1, -1, -1 }},
    {{ -1, -1,  1,  1 }},
    {{ -1, -1,  1, -1 }},
    {{ -1, -1, -1,  1 }},
    {{ -1, -1, -1, -1 }}
  };
  int p = 0;
  for (int dir = 0; dir < 16; ++dir) {
    for (int q = 0; q < 16; ++q, ++p) {
      for (int i = 0; i < 4; ++i) {
        d->points[p].v[i] = dirs[dir].v[i] * (q + 1) / 16;
      }
    }
  }
  return d;
}

static void h4tek_coords_free(struct h4tek_coords *d) {
  outlet_free(d->outlet);
}

extern void h4tek_coords_setup(void) {
  h4tek_coords_class = class_new(
    gensym("h4tek_coords"),
    (t_newmethod) h4tek_coords_new,
    (t_method) h4tek_coords_free,
    sizeof(struct h4tek_coords),
    CLASS_DEFAULT,
    0
  );
  class_addbang(h4tek_coords_class, h4tek_coords_bang);
  class_addmethod(h4tek_coords_class, (t_method) h4tek_coords_scale4,  gensym("scale4"),  A_GIMME, 0);
  class_addmethod(h4tek_coords_class, (t_method) h4tek_coords_rotate4, gensym("rotate4"), A_GIMME, 0);
  class_addmethod(h4tek_coords_class, (t_method) h4tek_coords_persp43, gensym("persp43"), A_GIMME, 0);
}
