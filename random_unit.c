#include <math.h>
#include <stdlib.h>
#include <m_pd.h>

/* generate vectors in cube, discard those outside sphere, normalize */
#define randf() (2.0 * (double) rand() / (double) RAND_MAX - 1.0)
inline void random_unit(float *v, const int n) {
  float r = 0.0;
  while (r < 0.00001 || 1.0 < r) {
    r = 0.0;
    for (int i = 0; i < n; ++i) {
      v[i] = randf();
      r += v[i] * v[i];
    }
  }
  r = sqrtf(r);
  for (int i = 0; i < n; ++i) {
    v[i] /= r;
  }
}
#undef randf

/* class pointer */
static t_class *random_unit_class;

/* how many dimensions should we handle at most */
#define RANDOM_UNIT_MAX 32

/* data structure */
struct random_unit {
  t_object obj;
  t_outlet *outlet;
  float buffer[RANDOM_UNIT_MAX];
  t_atom atoms[RANDOM_UNIT_MAX];
};

/* float method */
static void random_unit_float(struct random_unit *ru, t_floatarg f) {
  int n = f;
  if (0 < n && n <= RANDOM_UNIT_MAX) {
    random_unit(ru->buffer, n);
    for (int i = 0; i < n; ++i) {
      ru->atoms[i].a_w.w_float = ru->buffer[i];
    }
    outlet_list(ru->outlet, &s_list, n, ru->atoms);
  }
}

/* constructor */
static struct random_unit *random_unit_new(void) {
  struct random_unit *ru = (struct random_unit *) pd_new(random_unit_class);
  ru->outlet = outlet_new(&ru->obj, &s_list);
  return ru;
}

/* destructor */
static void random_unit_free(struct random_unit *ru) {
  outlet_free(ru->outlet);
}

/* setup */
extern void random_unit_setup(void) {
  random_unit_class = class_new(
    gensym("random_unit"),
    (t_newmethod) random_unit_new,
    (t_method) random_unit_free,
    sizeof(struct random_unit),
    CLASS_DEFAULT,
    0
  );
  class_addfloat(random_unit_class, random_unit_float);
}
