#include <math.h>
#include <string.h>
#include <m_pd.h>

#include "geometry.h"

/* class pointer */
static t_class *h4tek_24cell_class;

/* number of points in the 24-cell-based geometry */
#define h4tek_24cell_POINTS (24*16)

/* object data structure */
struct h4tek_24cell {
  t_object obj;
  t_outlet *outlet1;
  t_outlet *outlet2;
  float scale4[4];
  float persp43;
  struct quaternion q0;
  struct quaternion q1;
  struct quaternion p0;
  struct quaternion p1;
  struct vector4 points[h4tek_24cell_POINTS];
  struct vector3 points3[h4tek_24cell_POINTS];
};

/* vertex table */
struct vector4 h4tek_24cell_vertices[24] = {
  {{  1,  1,  0,  0 }}, /*  0 */
  {{ -1, -1,  0,  0 }}, /*  1 */
  {{  1, -1,  0,  0 }}, /*  2 */
  {{ -1,  1,  0,  0 }}, /*  3 */
  {{  1,  0,  1,  0 }}, /*  4 */
  {{ -1,  0, -1,  0 }}, /*  5 */
  {{  1,  0, -1,  0 }}, /*  6 */
  {{ -1,  0,  1,  0 }}, /*  7 */
  {{  1,  0,  0,  1 }}, /*  8 */
  {{ -1,  0,  0, -1 }}, /*  9 */
  {{  1,  0,  0, -1 }}, /* 10 */
  {{ -1,  0,  0,  1 }}, /* 11 */
  {{  0,  1,  1,  0 }}, /* 12 */
  {{  0, -1, -1,  0 }}, /* 13 */
  {{  0,  1, -1,  0 }}, /* 14 */
  {{  0, -1,  1,  0 }}, /* 15 */
  {{  0,  1,  0,  1 }}, /* 16 */
  {{  0, -1,  0, -1 }}, /* 17 */
  {{  0,  1,  0, -1 }}, /* 18 */
  {{  0, -1,  0,  1 }}, /* 19 */
  {{  0,  0,  1,  1 }}, /* 20 */
  {{  0,  0, -1, -1 }}, /* 21 */
  {{  0,  0,  1, -1 }}, /* 22 */
  {{  0,  0, -1,  1 }}  /* 23 */
};

/* edge struct */
struct h4tek_24cell_edge {
  int e[2];
};

/* edge table */
static struct h4tek_24cell_edge h4tek_24cell_edges[96] = {
  {{ 15, 79 }}, {{ 15, 111 }}, {{ 15, 143 }}, {{ 15, 175 }}, {{ 15, 207 }}, {{ 15, 239 }}, {{ 15, 271 }}, {{ 15, 303 }},
  {{ 31, 95 }}, {{ 31, 127 }}, {{ 31, 159 }}, {{ 31, 191 }}, {{ 31, 223 }}, {{ 31, 255 }}, {{ 31, 287 }}, {{ 31, 319 }},
  {{ 47, 79 }}, {{ 47, 111 }}, {{ 47, 143 }}, {{ 47, 175 }}, {{ 47, 223 }}, {{ 47, 255 }}, {{ 47, 287 }}, {{ 47, 319 }},
  {{ 63, 95 }}, {{ 63, 127 }}, {{ 63, 159 }}, {{ 63, 191 }}, {{ 63, 207 }}, {{ 63, 239 }}, {{ 63, 271 }}, {{ 63, 303 }},
  {{ 79, 143 }}, {{ 79, 175 }}, {{ 79, 207 }}, {{ 79, 255 }}, {{ 79, 335 }}, {{ 79, 367 }},
  {{ 95, 159 }}, {{ 95, 191 }}, {{ 95, 223 }}, {{ 95, 239 }}, {{ 95, 351 }}, {{ 95, 383 }},
  {{ 111, 143 }}, {{ 111, 175 }}, {{ 111, 223 }}, {{ 111, 239 }}, {{ 111, 351 }}, {{ 111, 383 }},
  {{ 127, 159 }}, {{ 127, 191 }}, {{ 127, 207 }}, {{ 127, 255 }}, {{ 127, 335 }}, {{ 127, 367 }},
  {{ 143, 271 }}, {{ 143, 319 }}, {{ 143, 335 }}, {{ 143, 383 }}, {{ 159, 287 }}, {{ 159, 303 }},
  {{ 159, 351 }}, {{ 159, 367 }}, {{ 175, 287 }}, {{ 175, 303 }}, {{ 175, 351 }}, {{ 175, 367 }},
  {{ 191, 271 }}, {{ 191, 319 }}, {{ 191, 335 }}, {{ 191, 383 }}, {{ 207, 271 }}, {{ 207, 303 }},
  {{ 207, 335 }}, {{ 207, 367 }}, {{ 223, 287 }}, {{ 223, 319 }}, {{ 223, 351 }}, {{ 223, 383 }},
  {{ 239, 271 }}, {{ 239, 303 }}, {{ 239, 351 }}, {{ 239, 383 }}, {{ 255, 287 }}, {{ 255, 319 }},
  {{ 255, 335 }}, {{ 255, 367 }}, {{ 271, 335 }}, {{ 271, 383 }}, {{ 287, 351 }}, {{ 287, 367 }},
  {{ 303, 351 }}, {{ 303, 367 }}, {{ 319, 335 }}, {{ 319, 383 }}
};

/* calculate transformation matrix and output transformed points */
static void h4tek_24cell_go1(struct h4tek_24cell *d, t_symbol *s, int count, t_atom *atoms) {
  float t;
  struct matrix4 n1, n2, n3;
  struct quaternion p, q;
  t = atoms[0].a_w.w_float;
  mdiag4(&n1, d->scale4);
  qslerp(&q, &d->q0, &d->q1, t);
  qslerp(&p, &d->p0, &d->p1, t);
  mquat2(&n2, &q, &p);
  mmul4m(&n3, &n2, &n1);
  for (int c = 0; c < h4tek_24cell_POINTS; ++c) {
    struct vector4 v4o;
    mmul4v(&v4o, &n3, &d->points[c]);
    struct vector3 v3;
    float z = d->persp43 + v4o.v[3];
    for (int k = 0; k < 3; ++k) {
      v3.v[k] = v4o.v[k] / z;
    }
    t_atom out[4];
    SETFLOAT(&out[0], v3.v[0]);
    SETFLOAT(&out[1], v3.v[1]);
    SETFLOAT(&out[2], v3.v[2]);
    SETFLOAT(&out[3], c);
    outlet_list(d->outlet2, &s_list, 4, out);
    d->points3[c].v[0] = v3.v[0];
    d->points3[c].v[1] = v3.v[1];
    d->points3[c].v[2] = v3.v[2];
  }
}

/* output edges */
static void h4tek_24cell_go2(struct h4tek_24cell *d, t_symbol *s, int count, t_atom *atoms) {
  for (int e = 0; e < 96; ++e) {
    t_atom out[6];
    SETFLOAT(&out[0], d->points3[h4tek_24cell_edges[e].e[0]].v[0]);
    SETFLOAT(&out[1], d->points3[h4tek_24cell_edges[e].e[0]].v[1]);
    SETFLOAT(&out[2], d->points3[h4tek_24cell_edges[e].e[0]].v[2]);
    SETFLOAT(&out[3], d->points3[h4tek_24cell_edges[e].e[1]].v[0]);
    SETFLOAT(&out[4], d->points3[h4tek_24cell_edges[e].e[1]].v[1]);
    SETFLOAT(&out[5], d->points3[h4tek_24cell_edges[e].e[1]].v[2]);
    outlet_list(d->outlet1, &s_list, 6, out);
  }
}

/* set 4d prescaling */
static void h4tek_24cell_scale4(
  struct h4tek_24cell *d, t_symbol *s, int count, t_atom *atoms
) {
  for (int i = 0; i < 4; ++i) {
    d->scale4[i] = atoms[i].a_w.w_float;
  }
}

/* set perspective depth */
static void h4tek_24cell_persp43(
  struct h4tek_24cell *d, t_symbol *s, int count, t_atom *atoms
) {
  d->persp43 = atoms[0].a_w.w_float;
}

/* set quaternion q0 */
static void h4tek_24cell_q0(
  struct h4tek_24cell *d, t_symbol *s, int count, t_atom *atoms
) {
  for (int i = 0; i < 4; ++i) {
    d->q0.q[i] = atoms[i].a_w.w_float;
  }
}

/* set quaternion q1 */
static void h4tek_24cell_q1(
  struct h4tek_24cell *d, t_symbol *s, int count, t_atom *atoms
) {
  for (int i = 0; i < 4; ++i) {
    d->q1.q[i] = atoms[i].a_w.w_float;
  }
}
/* set quaternion p0 */
static void h4tek_24cell_p0(
  struct h4tek_24cell *d, t_symbol *s, int count, t_atom *atoms
) {
  for (int i = 0; i < 4; ++i) {
    d->p0.q[i] = atoms[i].a_w.w_float;
  }
}

/* set quaternion p1 */
static void h4tek_24cell_p1(
  struct h4tek_24cell *d, t_symbol *s, int count, t_atom *atoms
) {
  for (int i = 0; i < 4; ++i) {
    d->p1.q[i] = atoms[i].a_w.w_float;
  }
}

/* initialize */
static struct h4tek_24cell *h4tek_24cell_new(void) {
  struct h4tek_24cell *d = (struct h4tek_24cell *) pd_new(h4tek_24cell_class);
  d->outlet1 = outlet_new(&d->obj, &s_list);
  d->outlet2 = outlet_new(&d->obj, &s_list);
  int p = 0;
  for (int dir = 0; dir < 24; ++dir) {
    for (int q = 0; q < 16; ++q, ++p) {
      for (int i = 0; i < 4; ++i) {
        d->points[p].v[i] = h4tek_24cell_vertices[dir].v[i] * (q + 1) / 16;
      }
    }
  }
  return d;
}

/* destructor */
static void h4tek_24cell_free(struct h4tek_24cell *d) {
  outlet_free(d->outlet2);
  outlet_free(d->outlet1);
}

/* setup */
extern void h4tek_24cell_setup(void) {
  h4tek_24cell_class = class_new(
    gensym("h4tek_24cell"),
    (t_newmethod) h4tek_24cell_new,
    (t_method) h4tek_24cell_free,
    sizeof(struct h4tek_24cell),
    CLASS_DEFAULT,
    0
  );
  class_addmethod(h4tek_24cell_class, (t_method) h4tek_24cell_go1, gensym("go1"), A_GIMME, 0);
  class_addmethod(h4tek_24cell_class, (t_method) h4tek_24cell_go2, gensym("go2"), A_GIMME, 0);
  class_addmethod(h4tek_24cell_class, (t_method) h4tek_24cell_scale4, gensym("scale4"), A_GIMME, 0);
  class_addmethod(h4tek_24cell_class, (t_method) h4tek_24cell_persp43, gensym("persp43"), A_GIMME, 0);
  class_addmethod(h4tek_24cell_class, (t_method) h4tek_24cell_q0, gensym("q0"), A_GIMME, 0);
  class_addmethod(h4tek_24cell_class, (t_method) h4tek_24cell_q1, gensym("q1"), A_GIMME, 0);
  class_addmethod(h4tek_24cell_class, (t_method) h4tek_24cell_p0, gensym("p0"), A_GIMME, 0);
  class_addmethod(h4tek_24cell_class, (t_method) h4tek_24cell_p1, gensym("p1"), A_GIMME, 0);
}
