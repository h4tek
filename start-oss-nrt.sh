#!/bin/bash
pd -stderr -oss -outchannels 2 \
-path /usr/lib/pd/extra/libstdcpp -lib libstdcpp \
-path /usr/lib/pd/extra/Gem -lib Gem \
-path /usr/lib/pd/extra/gridflow -lib gridflow \
-path /usr/lib/pd/extra/iemmatrix -lib iemmatrix \
-path /usr/lib/pd/extra/lua -lib lua \
-open h4tek_gui.pd -open h4tek_main.pd \
>> "h4tek-$(date --iso=s).log" 2>&1
