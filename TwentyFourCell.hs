module Main where

import Data.List(intersperse,nub,sort)

type Vertex = (Float,Float,Float,Float)

vertices = [
    (  1,  1,  0,  0 ),   --  0
    ( -1, -1,  0,  0 ),   --  1
    (  1, -1,  0,  0 ),   --  2
    ( -1,  1,  0,  0 ),   --  3
    (  1,  0,  1,  0 ),   --  4
    ( -1,  0, -1,  0 ),   --  5
    (  1,  0, -1,  0 ),   --  6
    ( -1,  0,  1,  0 ),   --  7
    (  1,  0,  0,  1 ),   --  8
    ( -1,  0,  0, -1 ),   --  9
    (  1,  0,  0, -1 ),   -- 10
    ( -1,  0,  0,  1 ),   -- 11
    (  0,  1,  1,  0 ),   -- 12
    (  0, -1, -1,  0 ),   -- 13
    (  0,  1, -1,  0 ),   -- 14
    (  0, -1,  1,  0 ),   -- 15
    (  0,  1,  0,  1 ),   -- 16
    (  0, -1,  0, -1 ),   -- 17
    (  0,  1,  0, -1 ),   -- 18
    (  0, -1,  0,  1 ),   -- 19
    (  0,  0,  1,  1 ),   -- 20
    (  0,  0, -1, -1 ),   -- 21
    (  0,  0,  1, -1 ),   -- 22
    (  0,  0, -1,  1 )    -- 23
  ]

lengths2 = nub . sort $ [ len2 x y | x <- vertices, y <- vertices ]

len2 (a,b,c,d) (w,x,y,z) = (a-w)*(a-w) + (b-x)*(b-x) + (c-y)*(c-y) + (d-z)*(d-z)

edges = [ (i, j) | i <- [0..23], j <- [i..23], let x = vertices !! i, let y = vertices !! j, len2 x y == 2 ]

pretty = concat . concat $ [
    ["h4tek_24cell_edges = {\n"],
    intersperse ", " $ map (\(i,j) -> "{{ " ++ show (i*16+15) ++ ", " ++ show (j*16+15) ++ " }}") edges,
    ["\n};"]
  ]

main = putStrLn pretty
