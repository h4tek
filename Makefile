CFLAGS = -std=c99 -pedantic -Wall -fPIC -I$(HOME)/include
OFLAGS = -O3 -funroll-loops -fstrength-reduce -fomit-frame-pointer \
-finline-functions -funsafe-loop-optimizations -Wunsafe-loop-optimizations \
-fstrict-aliasing -ffast-math -fsingle-precision-constant \
-march=pentium-m -mfpmath=sse,387 -msse -msse2
LFLAGS = -shared -lm

all: h4tek_24cell.pd_linux random_unit.pd_linux

# h4tek_coords.pd_linux

# vast-array-of-numbers.pd h2rgba.pd_linux colour.table h2rgba.table

h4tek_24cell.pd_linux: h4tek_24cell.c geometry.h Makefile
	gcc $(CFLAGS) $(OFLAGS) $(LFLAGS) -o h4tek_24cell.pd_linux h4tek_24cell.c

random_unit.pd_linux: random_unit.c Makefile
	gcc $(CFLAGS) $(OFLAGS) $(LFLAGS) -o random_unit.pd_linux random_unit.c

h4tek_coords.pd_linux: h4tek_coords.c
	gcc $(CFLAGS) $(OFLAGS) $(LFLAGS) -o h4tek_coords.pd_linux h4tek_coords.c

h2rgba.pd_linux: h2rgba.c
	gcc $(CFLAGS) $(OFLAGS) $(LFLAGS) -o h2rgba.pd_linux h2rgba.c

fast_hsv2rgb.pd_linux: fast_hsv2rgb.c
	gcc $(CFLAGS) $(OFLAGS) $(LFLAGS) -o fast_hsv2rgb.pd_linux fast_hsv2rgb.c

xyz2rgb.pd_linux: xyz2rgb.c
	gcc $(CFLAGS) $(OFLAGS) $(LFLAGS) -o xyz2rgb.pd_linux xyz2rgb.c

lab2xyz.pd_linux: lab2xyz.c
	gcc $(CFLAGS) $(OFLAGS) $(LFLAGS) -o lab2xyz.pd_linux lab2xyz.c

#d012345.pd_linux: d012345.c
#	gcc $(CFLAGS) $(OFLAGS) $(LFLAGS) -o d012345.pd_linux d012345.c

make-vast-array-of-numbers: make-vast-array-of-numbers.c
	gcc -std=c99 -pedantic -Wall -fPIC -O2 -o make-vast-array-of-numbers make-vast-array-of-numbers.c -lm

vast-array-of-numbers.pd: make-vast-array-of-numbers
	./make-vast-array-of-numbers >vast-array-of-numbers.pd

make-colour-table: make-colour-table.c
	gcc -std=c99 -pedantic -Wall -fPIC -O2 -o make-colour-table make-colour-table.c -lm

colour.table: make-colour-table
	./make-colour-table >colour.table

rainbow.pnm: rainbow.png
	pngtopnm rainbow.png >rainbow.pnm

#eqrainbow.png: eqrainbow.pnm
#	convert eqrainbow.pnm eqrainbow.png

#eqrainbow.pnm: rainbow.pnm eqrainbow
#	./eqrainbow <rainbow.pnm >eqrainbow.pnm

eqrainbow: eqrainbow.c
	gcc $(CFLAGS) -o eqrainbow eqrainbow.c -lm

rainbow.table: eqrainbow rainbow.pnm
	./eqrainbow <rainbow.pnm >rainbow.table

h2rgba.table: eqrainbow rainbow.pnm
	./eqrainbow <rainbow.pnm >h2rgba.table

