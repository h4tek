#ifndef GEOMETRY_H
#define GEOMETRY_H 1

#include <math.h>

/*====================================================================*\
** Vector and Matrix functions                                        **
\*====================================================================*/

/* 4-vector */
struct vector4 {
  float v[4];
};

/* 4x4-matrix */
struct matrix4 {
  float m[16];
};

/* 3-vector */
struct vector3 {
  float v[3];
};

/* matrix := diag(*f) */
inline void mdiag4(struct matrix4 *m, const float *f) {
  memset(m, 0, sizeof(struct matrix4));
  float *n = m->m;
  n[ 0] = f[0];
  n[ 5] = f[1];
  n[10] = f[2];
  n[15] = f[3];
}

/* matrix := rotation(i,j,angle) */
inline void mrot4(struct matrix4 *m, int i, int j, float a) {
  memset(m, 0, sizeof(struct matrix4));
  float *n = m->m;
  n[ 0] = 1.0;
  n[ 5] = 1.0;
  n[10] = 1.0;
  n[15] = 1.0;
  float c = cosf(a);
  float s = sinf(a);
  n[i*5]   =  c;
  n[j*5]   =  c;
  n[i*4+j] =  s;
  n[j*4+i] = -s;
}

/* matrix := matrix * matrix */
inline void mmul4m(
  struct matrix4 *m,
  const struct matrix4 *l,
  const struct matrix4 *r
) {
  for (int i = 0; i < 4; ++i) {
    for (int j = 0; j < 4; ++j) {
      float f = 0.0;
      for (int k = 0; k < 4; ++k) {
        f += l->m[i*4+k] * r->m[k*4+j];
      }
      m->m[i*4+j] = f;
    }
  }
}

/* vector := matrix * vector */
inline void mmul4v(
  struct vector4 *v,
  const struct matrix4 *l,
  const struct vector4 *r
) {
  for (int i = 0; i < 4; ++i) {
    float f = 0.0;
    for (int j = 0; j < 4; ++j) {
      f += l->m[i*4+j] * r->v[j];
    }
    v->v[i] = f;
  }
}

/*====================================================================*\
** Quaternion functions                                               **
** -------------------------------------------------------------------**
** http://www.lce.hut.fi/~ssarkka/pub/quat.pdf                        **
\*====================================================================*/

/* quaternion data structure */
struct quaternion {
  float q[4];
};

/* quaternion norm squared */
inline float qnorm2(const struct quaternion *p) {
  float p1, p2, p3, p4, r;
  p1 = p->q[0]; p2 = p->q[1]; p3 = p->q[2]; p4 = p->q[3];
  r = p1*p1 + p2*p2 + p3*p3 + p4*p4;
  return r;
}

/* quaternion norm */
inline float qnorm(const struct quaternion *p) {
  return sqrtf(qnorm2(p));
}

/* quaternion conjugate: r := p* */
inline void qconj(struct quaternion *r, const struct quaternion *p) {
  r->q[0] =  p->q[0];
  r->q[1] = -p->q[1];
  r->q[2] = -p->q[2];
  r->q[3] = -p->q[3];
}

/* quaternion inverse: r := p^-1 */
/* p^-1 = p* / |p|^2 */
inline void qinv(struct quaternion *r, const struct quaternion *p) {
  float pn2 = qnorm2(p);
  r->q[0] =  p->q[0] / pn2;
  r->q[1] = -p->q[1] / pn2;
  r->q[2] = -p->q[2] / pn2;
  r->q[3] = -p->q[3] / pn2;  
}

/* quaternion addition: r := q + p */
inline void qadd(
  struct quaternion *r,
  const struct quaternion *q,
  const struct quaternion *p
) {
  float q1, q2, q3, q4, p1, p2, p3, p4, r1, r2, r3, r4;
  q1 = q->q[0]; q2 = q->q[1]; q3 = q->q[2]; q4 = q->q[3];
  p1 = p->q[0]; p2 = p->q[1]; p3 = p->q[2]; p4 = p->q[3];
  r1 = q1 + p1;
  r2 = q2 + p2;
  r3 = q3 + p3;
  r4 = q4 + p3;
  r->q[0] = r1; r->q[1] = r2; r->q[2] = r3; r->q[3] = r4;
}

/* quaternion multiplication: r := q * p */
inline void qmul(
  struct quaternion *r,
  const struct quaternion *q,
  const struct quaternion *p
) {
  float q1, q2, q3, q4, p1, p2, p3, p4, r1, r2, r3, r4;
  q1 = q->q[0]; q2 = q->q[1]; q3 = q->q[2]; q4 = q->q[3];
  p1 = p->q[0]; p2 = p->q[1]; p3 = p->q[2]; p4 = p->q[3];
  r1 = q1 * p1 - q2 * p2 - q3 * p3 - q4 * p4;
  r2 = q2 * p1 + q1 * p2 - q4 * p3 + q3 * p4;
  r3 = q3 * p1 + q4 * p2 + q1 * p3 - q2 * p4;
  r4 = q4 * p1 - q3 * p2 + q2 * p3 + q1 * p4;
  r->q[0] = r1; r->q[1] = r2; r->q[2] = r3; r->q[3] = r4;
}

/* quaternion exponentiation: r := exp(p) */
/* exp(s;v) = exp(s) (cos(|v|) ; v sin(|v|) / |v|) */
inline void qexp(struct quaternion *r, const struct quaternion *p) {
  float s, v0, v1, v2, vn, se, vns, vnc;
  s   = p->q[0];
  v0  = p->q[1];
  v1  = p->q[2];
  v2  = p->q[3];
  vn  = sqrtf(v0*v0 + v1*v1 + v2*v2);
  vnc = cosf(vn);
  vns = sinf(vn) / vn;
  se  = expf(s);
  r->q[0] = se * vnc;
  r->q[1] = v0 * vns;
  r->q[2] = v1 * vns;
  r->q[3] = v2 * vns;
}

/* quaternion logarithm: r := log(p) */
/* ln(q@(s;v)) = (ln(|q|) ; v acos(s / |q|) / |v|) */
inline void qlog(struct quaternion *r, const struct quaternion *p) {
  float s, v0, v1, v2, vn2, vn, qn, qnl, sac;
  s   = p->q[0];
  v0  = p->q[1];
  v1  = p->q[2];
  v2  = p->q[3];
  vn2 = v0*v0 + v1*v1 + v2*v2;
  vn  = sqrtf(vn2);
  qn  = sqrtf(s*s + vn2);
  qnl = logf(qn);
  sac = acosf(s / qn) / vn;
  r->q[0] = qnl;
  r->q[1] = v0 * sac;
  r->q[2] = v1 * sac;
  r->q[3] = v2 * sac;
}

/* quaternion scalar power: r := p^f */
/* p^f = exp(ln(p) f) */
inline void qpows(
  struct quaternion *r,
  const struct quaternion *p,
  float f
) {
  struct quaternion l;
  qlog(&l, p);
  l.q[0] *= f;
  l.q[1] *= f;
  l.q[2] *= f;
  l.q[3] *= f;
  qexp(r, &l);
}

/* quaternion slerp: r := Slerp(p,q,t) */
/* Slerp(p,q;t) = (q p^-1)^t p */
inline void qslerp(
  struct quaternion *r,
  const struct quaternion *p,
  const struct quaternion *q,
  float t
) {
  struct quaternion p1, qp1, qp1t;
  qinv(&p1, p);
  qmul(&qp1, q, &p1);
  qpows(&qp1t, &qp1, t);
  qmul(r, &qp1t, p);
}

/*====================================================================*\
** Quaternions to 4D Rotation Matrix                                  **
** -------------------------------------------------------------------**
** http://www.cs.indiana.edu/pub/techreports/TR406.pdf                **
\*====================================================================*/

/* 4x4 rotation matrix from two unit(?) quaternions */
inline void mquat2(
  struct matrix4 *m,
  const struct quaternion *q,
  const struct quaternion *p
) {
  float q0, q1, q2, q3, p0, p1, p2, p3;
  q0 = q->q[0]; q1 = q->q[1]; q2 = q->q[2]; q3 = q->q[3];
  p0 = p->q[0]; p1 = p->q[1]; p2 = p->q[2]; p3 = p->q[3];

  m->m[ 0] =   q0*p0 + q1*p1 + q2*p2 + q3*p3;
  m->m[ 1] =   q1*p0 - q0*p1 - q3*p2 + q2*p3;
  m->m[ 2] =   q2*p0 + q3*p1 - q0*p2 - q1*p3;
  m->m[ 3] =   q3*p0 - q2*p1 + q1*p2 - q0*p3;

  m->m[ 4] = - q1*p0 + q0*p1 - q3*p2 + q2*p3;
  m->m[ 5] =   q0*p0 + q1*p1 - q2*p2 - q3*p3;
  m->m[ 6] = - q3*p0 + q2*p1 + q1*p2 - q0*p3;
  m->m[ 7] =   q2*p0 + q3*p1 + q0*p2 + q1*p3;

  m->m[ 8] = - q2*p0 + q3*p1 + q0*p2 - q1*p3;
  m->m[ 9] =   q3*p0 + q2*p1 + q1*p2 + q0*p3;
  m->m[10] =   q0*p0 - q1*p1 + q2*p2 - q3*p3;
  m->m[11] = - q1*p0 - q0*p1 + q3*p2 + q2*p3;

  m->m[12] = - q3*p0 - q2*p1 + q1*p2 + q0*p3;
  m->m[13] = - q2*p0 + q3*p1 - q0*p2 + q1*p3;
  m->m[14] =   q1*p0 + q0*p1 + q3*p2 + q2*p3;
  m->m[15] =   q0*p0 - q1*p1 - q2*p2 + q3*p3;

}

#endif
